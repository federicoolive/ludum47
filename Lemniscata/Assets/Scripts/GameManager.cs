﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class GameManager : MonoBehaviour
{

    [SerializeField] DoorScript[] Doors;
    [SerializeField] CapsuleCollider[] WagonStartColliders;
    [SerializeField] GameObject player;
    [SerializeField] Vector3 wagonOffset = new Vector3(-750, 0, 0);
    [SerializeField] int currentWagon = 1;

    private void Start()
    {
        for (int i = 0; i < Doors.Length; i++)
        {
            Doors[i].enabled = true;
        }
    }

    public void NextWagon()
    {
        switch (currentWagon)
        {
            case 1:
                Doors[1].ToogleDoor();
                Doors[2].ToogleDoor();
                break;
            case 2:
                Doors[3].ToogleDoor();
                Doors[4].ToogleDoor();
                break;
            case 3:
                Doors[5].ToogleDoor();
                Doors[6].ToogleDoor();
                break;
            case 4:
                Doors[7].ToogleDoor();
                Doors[0].ToogleDoor();
                break;
        }
        currentWagon++;
        if (currentWagon > 4)
        {
            currentWagon = 1;
        }
    }

    public void PuzzleComplete()
    {
        switch (currentWagon)
        {
            case 1:
                Doors[0].ToogleDoor();
                Doors[1].ToogleDoor();
                WagonStartColliders[0].enabled = true;
                break;
            case 2:
                Doors[2].ToogleDoor();
                Doors[3].ToogleDoor();
                WagonStartColliders[1].enabled = true;
                break;
            case 3:
                Doors[4].ToogleDoor();
                Doors[5].ToogleDoor();
                WagonStartColliders[2].enabled = true;
                break;
            case 4:
                Doors[6].ToogleDoor();
                Doors[7].ToogleDoor();
                WagonStartColliders[3].enabled = true;
                break;
        }
    }

    private void ToogleDoor(int id)
    {
        Doors[id].ToogleDoor();
    }
}
