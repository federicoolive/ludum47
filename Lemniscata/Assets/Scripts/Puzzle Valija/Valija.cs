﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Valija : MonoBehaviour
{
    public GameObject tanga;
    public GameObject libro;
    public GameObject lentes;
    public GameObject camisa;
    public GameObject tapamaleta;

    public bool checkTanga;
    public bool checkLibro;
    public bool checkLentes;
    public bool checkCamisa;

    public bool puzzleValijaTerminado = false;

    // Start is called before the first frame update
    void Start()
    {
        checkTanga = false;
        checkLibro = false;
        checkLentes = false;
        checkCamisa = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (checkTanga && checkLibro && checkLentes && checkCamisa && !puzzleValijaTerminado)
        {
            puzzleValijaTerminado = true;
            tapamaleta.GetComponent<Animator>().SetBool("NivelCompletado", puzzleValijaTerminado);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Tanga")
        {
            checkTanga = true;
            Debug.Log("Tanga colision");
        }
        if (other.gameObject.name == "Camisa")
        {
            checkCamisa = true;
            Debug.Log("Camisa colision");
        }
        if (other.gameObject.name == "Libro")
        {
            checkLibro = true;
            Debug.Log("Libro colision");
        }
        if (other.gameObject.name == "Lentes")
        {
            checkLentes = true;
            Debug.Log("Lentes colision");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name == "Tanga")
        {
            checkTanga = false;
            puzzleValijaTerminado = false;
            tapamaleta.GetComponent<Animator>().SetBool("NivelCompletado", puzzleValijaTerminado);
        }
        if (other.gameObject.name == "Camisa")
        {
            checkCamisa = false;
            puzzleValijaTerminado = false;
            tapamaleta.GetComponent<Animator>().SetBool("NivelCompletado", puzzleValijaTerminado);
        }
        if (other.gameObject.name == "Libro")
        {
            checkLibro = false;
            puzzleValijaTerminado = false;
            tapamaleta.GetComponent<Animator>().SetBool("NivelCompletado", puzzleValijaTerminado);
        }
        if (other.gameObject.name == "Lentes")
        {
            checkLentes = false;
            puzzleValijaTerminado = false;
            tapamaleta.GetComponent<Animator>().SetBool("NivelCompletado", puzzleValijaTerminado);
        }
    }
}