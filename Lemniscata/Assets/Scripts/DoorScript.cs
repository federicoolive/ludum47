﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorScript : MonoBehaviour
{

    bool doorOpen = false;
    [SerializeField] float doorOpenSpeed = 1f;
    [SerializeField] float doorPositionMovement = -1.5f;
    Collider col;
    Vector3 target;
    Vector3 startingPos;

    public void ToogleDoor()
    {
        doorOpen = !doorOpen;
        if (doorOpen)
        {
            target = transform.position + new Vector3(0, 0, doorPositionMovement);
        }
        else
        {
            target = startingPos;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        col = GetComponent<Collider>();
        target = transform.position;
        startingPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
            transform.position = Vector3.Lerp(transform.position, target, doorOpenSpeed * Time.deltaTime);
    }
}
