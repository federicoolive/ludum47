﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonPuzzle : MonoBehaviour
{

    [SerializeField] GameObject[] buttons;

    // Start is called before the first frame update
    public void ButtonPress(int id)
    {
        switch (id)
        {
            case 1:
                buttons[id].GetComponentInChildren<ButtonAction>().SwitchButton();
                buttons[id - 1].GetComponentInChildren<ButtonAction>().SwitchButton();
                break;
            case 2:
            case 3:
            case 4:
                buttons[id].GetComponentInChildren<ButtonAction>().SwitchButton();
                buttons[id - 1].GetComponentInChildren<ButtonAction>().SwitchButton();
                buttons[id - 2].GetComponentInChildren<ButtonAction>().SwitchButton();
                break;
            case 5:
                buttons[id - 1].GetComponentInChildren<ButtonAction>().SwitchButton();
                buttons[id - 2].GetComponentInChildren<ButtonAction>().SwitchButton();
                break;
            default:
                break;
        }
    }
}
