﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonAction : Action
{

    [SerializeField] int id = 0;
    private bool buttonOn = false;
    [SerializeField] Material offMaterial;
    [SerializeField] Material onMaterial;

    // Start is called before the first frame update
    override public void DoAction()
    {

        GetComponentInParent<ButtonPuzzle>().ButtonPress(id);    

    }

    public void SwitchButton()
    {
        buttonOn = !buttonOn;
        if (buttonOn)
        {
            GetComponent<Renderer>().material = onMaterial;
        }
        else
        {
            GetComponent<Renderer>().material = offMaterial;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
