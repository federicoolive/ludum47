﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Back : MonoBehaviour
{
    public GameObject MenuPrincipal;
    public GameObject MenuOptions;
    public GameObject MenuCredits;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnMouseDown()
    {
        MenuPrincipal.SetActive(true);
        MenuOptions.SetActive(false);
        MenuCredits.SetActive(false);
    }
}
