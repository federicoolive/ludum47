﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Play : MonoBehaviour
{
    public GameObject celular;
    public PlayerMovement script;

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    public void OnMouseDown()
    {
        Cursor.lockState = CursorLockMode.Locked;
        celular.GetComponent<Animator>().SetBool("Cerrar", true);
        Invoke("ApagarCelular", 1);
        script.playerMovement = true;
    }
    void ApagarCelular()
    {
        celular.SetActive(false);
    }
}
