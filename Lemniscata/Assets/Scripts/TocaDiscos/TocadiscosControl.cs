﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TocadiscosControl : Action
{
    // Start is called before the first frame update
    bool open = false;
    Animator tocadiscosAnim;
    [SerializeField] Animator discosAnim;
    [SerializeField] DiscosScript[] discs;
    [SerializeField] int[] diskCode;
    float diskOffset = 0.2f;

    int selectedDisk = -1;

    private void Start()
    {
        tocadiscosAnim = GetComponent<Animator>();
    }

    
    private void OnTriggerEnter(Collider other)
    {
        open = !open;
        tocadiscosAnim.SetBool("Open", open);
        discosAnim.SetBool("Open", open);
        if (CheckForComplete())
        {
            Debug.Log("Puzzle Complete");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        open = !open;
        tocadiscosAnim.SetBool("Open", open);
        discosAnim.SetBool("Open", open);
        if (CheckForComplete())
        {
            Debug.Log("Puzzle Complete");
        }
    }

    public void SelectDisk(int position)
    {
        if (selectedDisk == -1)
        {
            selectedDisk = position;
            discs[selectedDisk].transform.Translate(Vector3.back * diskOffset);
        }
        else if (selectedDisk == position)
        {
            discs[selectedDisk].transform.Translate(Vector3.forward * diskOffset);
            selectedDisk = -1;
        }
        else
        {
            if (selectedDisk > position)
            {
                discs[selectedDisk].transform.Translate(Vector3.up * diskOffset * (selectedDisk - position));
                discs[selectedDisk - 1 * (selectedDisk - position)].transform.Translate(Vector3.down * diskOffset * (selectedDisk - position));
            }
            else
            {
                discs[selectedDisk].transform.Translate(Vector3.down * diskOffset * (position - selectedDisk));
                discs[selectedDisk + 1 * (position - selectedDisk)].transform.Translate(Vector3.up * diskOffset * (position - selectedDisk));

            }
            var aux = discs[position];
            discs[position] = discs[selectedDisk];
            discs[selectedDisk] = aux;
            var aux2 = discs[position].position;
            discs[position].position = discs[selectedDisk].position;
            discs[selectedDisk].position = aux2;
            selectedDisk = position;
        }
    }

    bool CheckForComplete()
    {
        bool aux = true;
        for(int i = 0; i < diskCode.Length; i++)
        {
            if(diskCode[i] != discs[i].GetId())
            {
                aux = false;
                break;
            }
        }
        return aux;
    }

}
