﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiscosScript : Action
{
    // Start is called before the first frame update
    [SerializeField] TocadiscosControl tControl;
    [SerializeField] GameObject etiqueta;
    [SerializeField] int id = 1;
    public int position = 1;
    float moveAmmount = 0.25f;
    bool selected = false;

    public override void DoAction()
    {
        tControl.SelectDisk(position);
    }

    public int GetId()
    {
        return id;
    }

}
