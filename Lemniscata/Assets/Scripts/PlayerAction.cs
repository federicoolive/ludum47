﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAction : MonoBehaviour
{

    Action action;

    // Start is called before the first frame update
    void Start()
    {
        action = GetComponent<Action>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Interact()
    {

        action.DoAction();

    }

    public void InteractLeft()
    {

        action.QButton();

    }

    public void InteractRight()
    {

        action.EButton();

    }

}
