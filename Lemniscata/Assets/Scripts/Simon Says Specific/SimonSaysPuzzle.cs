﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimonSaysPuzzle : MonoBehaviour
{

    [SerializeField] SimonSaysButtonAction[] buttons;
    [SerializeField] int PuzzleRounds = 5;
    [SerializeField] Material rightColor;
    [SerializeField] Material wrongColor;
    [SerializeField] float timeBetweenActions = 1.5f;
    [SerializeField] int[] currentNumbers;
    [SerializeField] int[] playerInput;

    private int currentRound = 1;
    private int currentPlayerInput = 0;
    bool inputEnabled = true;
    bool puzzleOn = false;
    bool puzzleWon = false;

    public void ButtonPress(int id)
    {
        if (puzzleOn && inputEnabled)
        {
            switch (id)
            {
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                    playerInput[currentPlayerInput] = id;
                    buttons[id].TurnWithTime(timeBetweenActions / 2);
                    Debug.Log("Player interacted with button: " + id);
                    currentPlayerInput++;
                    break;
                default:
                    break;
            }
            if (currentPlayerInput >= currentRound)
            {
                bool continuePlaying = true;
                for (int i = 0; i < currentRound; i++)
                {
                    if (playerInput[i] != currentNumbers[i])
                    {
                        Debug.Log("Player input: " + playerInput[i] + " Current number: " + currentNumbers[i]);
                        continuePlaying = false;
                    }
                }
                if (continuePlaying)
                {
                    currentRound++;
                    if (currentRound == PuzzleRounds)
                    {
                        PuzzleWon();
                    }
                    else
                    {
                        StartCoroutine(CoroutineContinuePuzzle());
                    }
                }
                else
                {
                    RestartPuzzle();
                }
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<PlayerMovement>())
        {
            RestartPuzzle();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.GetComponent<PlayerMovement>())
        {
            DisablePuzzle();
        }
    }

    void DisablePuzzle()
    {
        if (!puzzleWon)
        {
            for (int i = 0; i < buttons.Length; i++)
            {
                buttons[i].StopButton();
                puzzleOn = false;
                StopAllCoroutines();
            }
        }
    }

    private void PuzzleWon()
    {
        puzzleOn = false;
        puzzleWon = true;
        StartCoroutine(CoroutinePuzzleWin());
    }
    private void RestartPuzzle()
    {
        puzzleOn = true;
        currentRound = 1;
        currentPlayerInput = 0;
        currentNumbers = new int[PuzzleRounds];
        playerInput = new int[PuzzleRounds];

        for (int i = 0; i < PuzzleRounds; i++)
        { 
            currentNumbers[i] = Random.Range(1, 9);
            playerInput[i] = 0;
        }

        inputEnabled = false;
        StopAllCoroutines();
        StartCoroutine(RestartPuzzleCouroutine());

    }

    IEnumerator CoroutinePuzzleWin()
    {
        yield return new WaitForSeconds(timeBetweenActions);
        for (int i = 0; i < buttons.Length; i++)
        {
            buttons[i].GetComponent<Renderer>().material = rightColor;
            buttons[i].Deactivate();
        }
    }

    IEnumerator CoroutineContinuePuzzle()
    {
        currentPlayerInput = 0;
        inputEnabled = false;
        yield return new WaitForSeconds(timeBetweenActions * 2);
        for (int i = 0; i < currentRound; i++)
        {
            playerInput[i] = 0;
            buttons[currentNumbers[i]].TurnWithTime(timeBetweenActions / 2);
            yield return new WaitForSeconds(timeBetweenActions);
        }
        inputEnabled = true;
    }

    IEnumerator RestartPuzzleCouroutine()
    {
        for(int i = 0; i < buttons.Length; i++)
        {
            buttons[i].GetComponent<Renderer>().material = wrongColor;
        }
        yield return new WaitForSeconds(timeBetweenActions * PuzzleRounds);
        for (int i = 0; i < buttons.Length; i++)
        {
            buttons[i].StopButton();
        }
        for (int i = 0; i < currentRound; i++)
        {
            buttons[currentNumbers[i]].TurnWithTime(timeBetweenActions / 2);
            yield return new WaitForSeconds(timeBetweenActions);
        }
        inputEnabled = true;
    }

}
