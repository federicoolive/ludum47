﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimonSaysButtonAction : Action
{

    [SerializeField] int id = 0;
    [SerializeField] Material offMaterial;
    [SerializeField] Material onMaterial;
    bool activated = true;

    override public void DoAction()
    {
        GetComponentInParent<SimonSaysPuzzle>().ButtonPress(id);
    }

    public void Deactivate()
    {
        activated = false;
    }

    public void TurnWithTime(float time)
    {
        if (activated)
        {
            Debug.Log("Starting button:" + id);
            StartCoroutine(PickCoroutine(time));
        }
    }

    public void StopButton()
    {
        GetComponent<Renderer>().material = offMaterial;
    }
    public void StartButton()
    {
        GetComponent<Renderer>().material = onMaterial;
    }

    IEnumerator PickCoroutine(float time)
    {
        GetComponent<Renderer>().material = onMaterial;
        yield return new WaitForSeconds(time);
        GetComponent<Renderer>().material = offMaterial;
    }


}
