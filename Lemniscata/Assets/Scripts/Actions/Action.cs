﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Action : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public virtual void DoAction()
    {
        return;
    }

    public virtual void QButton()
    {
        return;
    }

    public virtual void EButton()
    {
        return;
    }

}
