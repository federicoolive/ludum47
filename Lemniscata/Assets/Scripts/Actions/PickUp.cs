﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class PickUp : Action
{
    // Start is called before the first frame update

    [SerializeField] Transform cameraPos;
    [SerializeField] float distanceToPlayer = 10;
    [SerializeField] float stepsDistance = .01f;
    [SerializeField] float rotationSpeed = 1f;

    bool pickedUp = false;
    bool unPickcable = false;
    Vector3 Target = Vector3.zero;
    Quaternion lockedRotation;
    Rigidbody rb;


    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    public void MakeUnpickable()
    {
        unPickcable = true;
    }

    public bool GetPickedUp()
    {
        return pickedUp;
    }

    public void SetPickedUp(bool isPicked)
    {
        pickedUp = isPicked;
    }

    override public void DoAction()
    {
        if (!unPickcable)
        {
            pickedUp = !pickedUp;
            if (pickedUp)
            {
                rb.constraints = RigidbodyConstraints.FreezeRotation;
                GetComponent<Rigidbody>().useGravity = false;
            }
            else
            {
                rb.constraints = RigidbodyConstraints.None;
                GetComponent<Rigidbody>().useGravity = true;
            }
        }
    }

    override public void QButton()
    {

        if (pickedUp)
        {
            transform.Rotate(transform.up, -rotationSpeed);
            Debug.Log("Rotation left");
        }

    }

    override public void EButton()
    {

        if (pickedUp)
        {
            transform.Rotate(transform.up, rotationSpeed);
            Debug.Log("Rotation right");
        }

    }

    private void Update()
    {
        if (pickedUp)
        {
            rb.MovePosition(cameraPos.transform.position + cameraPos.forward * distanceToPlayer);
        }
    }

}
