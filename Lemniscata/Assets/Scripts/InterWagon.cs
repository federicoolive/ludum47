﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InterWagon : MonoBehaviour
{

    [SerializeField] GameManager gm;
    CapsuleCollider col;

    public void colliderEnable()
    {
        col.enabled = true;
    }

    private void Start()
    {
        col = GetComponent<CapsuleCollider>();
        col.enabled = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        gm.NextWagon();
        col.enabled = false;
    }

}
