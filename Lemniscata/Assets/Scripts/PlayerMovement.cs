﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class PlayerMovement : MonoBehaviour
{

    [SerializeField] float movementSpeed = 10;

    [SerializeField] Camera cam;
    [SerializeField] float cameraMaxUp = 30f;
    [SerializeField] float cameraMaxDown = -30f;
    [SerializeField] float yCamSensitivity = 1.5f;
    [SerializeField] float xCamSensitivity = 1.5f;

    Rigidbody rb;
    CinemachineVirtualCamera vc;
    float cameraRotationX;
    float cameraRotationY;
    public bool playerMovement = false;

    public Play scriptPlay;
    public GameObject celular;

    void Start()
    {
        Cursor.lockState = CursorLockMode.None;
        playerMovement = false;
        rb = GetComponent<Rigidbody>();
        vc = GetComponentInChildren<CinemachineVirtualCamera>();
    }

    void FixedUpdate()
    {
        if (playerMovement)
        {

            Vector3 velocity = Vector3.zero;
            velocity += vc.transform.forward * Input.GetAxis("Vertical") * movementSpeed;
            velocity += vc.transform.right * Input.GetAxis("Horizontal") * movementSpeed;
            rb.velocity = velocity;

            RaycastHit hit;
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
            {
                if (Input.GetButtonDown("Interact") && hit.collider.gameObject.GetComponent<PlayerAction>())
                {
                    Debug.Log("Interact");
                    hit.collider.gameObject.GetComponent<PlayerAction>().Interact();
                }
                else if (Input.GetButton("QButton") && hit.collider.gameObject.GetComponent<PlayerAction>())
                {
                    hit.collider.gameObject.GetComponent<PlayerAction>().InteractLeft();
                    Debug.Log("Rotation Left");
                }
                else if (Input.GetButton("EButton") && hit.collider.gameObject.GetComponent<PlayerAction>())
                {
                    hit.collider.gameObject.GetComponent<PlayerAction>().InteractRight();
                    Debug.Log("Rotation Right");
                }
            }
            cameraRotationX += Input.GetAxis("Mouse X") * xCamSensitivity;
            cameraRotationY += Input.GetAxis("Mouse Y") * yCamSensitivity;
            cameraRotationY = Mathf.Clamp(cameraRotationY, cameraMaxDown, cameraMaxUp);
            //transform.localEulerAngles = new Vector3(0, cameraRotationY, 0);
            vc.transform.localEulerAngles = new Vector3(-cameraRotationY, cameraRotationX, 0);
        }

        if (Input.GetButtonDown("Jump"))
        {
            if (celular.gameObject.activeSelf)
            {
                
                scriptPlay.OnMouseDown();
            }
            else
            {
                Cursor.lockState = CursorLockMode.None;
                celular.SetActive(true);
                playerMovement = false;
                rb.velocity = Vector3.zero;
            }
        }
    }
}

