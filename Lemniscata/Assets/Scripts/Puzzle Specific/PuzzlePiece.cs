﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzlePiece : MonoBehaviour
{

    [SerializeField] int id = 0;
    bool completed = false;
    
    public int getId()
    {
        return id;
    }

    public bool GetCompleted()
    {
        return completed;
    }

    public void SetCompleted(bool completeState)
    {
        completed = completeState;
    }

}
