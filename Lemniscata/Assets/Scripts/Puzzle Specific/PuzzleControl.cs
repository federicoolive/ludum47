﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleControl : MonoBehaviour
{

    [SerializeField] GameManager gm;
    [SerializeField] int PiecesAmount;
    int currentPiecesComplete = 0;

    private void Update()
    {
        if(currentPiecesComplete == PiecesAmount)
        {
            this.enabled = false;
            gm.PuzzleComplete();
        }
    }

    public void AddPiece()
    {
        currentPiecesComplete++;
    }


}
