﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleSlot : MonoBehaviour
{

    [SerializeField] int id = 0;
    [SerializeField] float position = 0.025f;
    [SerializeField] float raySize = .01f;
    [SerializeField] PuzzleControl puzzleControl;

    Ray raycastRay;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.position - Vector3.right * raySize, out hit, raySize))
        {
            if (hit.collider.gameObject.layer == 8 && hit.collider.GetComponent<PuzzlePiece>() && !hit.collider.GetComponent<PuzzlePiece>().GetCompleted()) //Layer 8 = Slots
            {
                hit.collider.gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
                var hitGameObject = hit.collider.gameObject;

                Debug.Log("Pieza Encontrada");

                if (hitGameObject.GetComponent<PuzzlePiece>().getId() == id)
                {
                    Debug.Log("Pieza correcta encontrada");
                    hitGameObject.GetComponent<PickUp>().SetPickedUp(false);
                    hitGameObject.GetComponent<PickUp>().MakeUnpickable();
                    hitGameObject.GetComponent<PuzzlePiece>().SetCompleted(true);
                    hitGameObject.transform.position = transform.position - Vector3.right * position;
                    hitGameObject.transform.rotation = transform.rotation;
                    hitGameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
                    puzzleControl.AddPiece();
                }

            }
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawLine(transform.position, transform.position - Vector3.right * raySize);
    }

}
