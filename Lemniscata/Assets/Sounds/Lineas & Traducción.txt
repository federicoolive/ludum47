line 00 BAR : "All the drinks are alcohol free, I'm sorry if you thought you could do anything about it"
//
Todas las bebidas son sin alcohol, perdon si pensaste que ibas a poder hacer algo al respecto

line 00 BOVEDA : "This place is so nasty, why dont you clean a little? That will make you so much 
more usefull than wathever you've been doing."
// 
Este lugar esta tan sucio, por qué no limpias un poco? Eso te va a hacer mucho mas útil que lo que 
sea que estuviste haiendo.

line 00 CUARTO : "Don't even think about going to sleep now, we're getting closer to get you away from me
for every room you complete." 
//
Ni siquiera pienses en irte a dormir ahora, nos estamos acercando cada vez más a alejarte de mi por
cada habitación que completas.

line 00 MUSICAL : "Hope you like music, you are going to hear it a long time."
//
Espero que te guste la música, vas a escucharla un tiempo largo.

line 00 MEDICO : "This is the place where we make experiments, fell free to drink anything. 
Especially bright green things"
//
Este es el lugar en donde hacemos esperimentos, sentite libre de beber cualquier cosa. Especialmente
cosas verdes y brillantes.

line 01 : "I can't belive you didn't solve it/figured it out yet" 
//
no puedo creer que no lo resolviste todavía.

line 02 : "Are you always this slow?"
//
sos siempre así de lento?

line 03 : "*sigh* it's not that way"
//
*suspiro* no es por ahí.

line 04 : "Can you do it faster please? i have more interesting things to do than
watch you touch everything and wander around."
//
podes hacerlo más rapido por favor? tengo que hacer cosas más interesantes que mirarte
tocanto todo y deambular.

line 05 : "Finally!"  
// 
finalmente!

line 06 : "Let's see how much time this one takes you" 
//
Vamos a ver cuanto tiempo te lleva este.

line 07 : "*yawn* I'm getting bored right here"
//
*bostezo* me estoy aburriendo por acá

line 08 : "I must say I didn't belive you would get this far, we'll see how long you'll last"
//
Debo decir que no pensé que llegarías tan lejos, vamos a ver cuanto durás

line 09 : "Still here? Why don't you close the game and give me a break already?"
//
Seguis acá? por qué no cerras el juego y me dejas en paz de una vez?

line 10 : "Do you want me to help you? ...haha just kidding I don't like you that much"
//
Queres que te ayude? ...jaja solo bromeaba, no me agradas tanto

line 11 : "I'm still thinking this is not for you but go on I guess..." 
//
Sigo pensando que esto no es para vos, pero seguí creo...

line 12 : "Weren't you here like five times already?"
//
¿No estuviste aquí como cinco veces ya?

line 13 : "Even a toddler would've fidured it out by now, that's very sad, don't you think?"
//
Hasta un infante lo habría resulto para este momento, eso es bastante triste, no lo crees?